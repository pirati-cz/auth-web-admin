/* global marked */
import React from 'react'
import {observer} from 'mobx-react'

const MDPreview = ({attr, record}) => (
  <span dangerouslySetInnerHTML={{__html: marked(record.get(attr))}} />
)

export default observer(MDPreview)
