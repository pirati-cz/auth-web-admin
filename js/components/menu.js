/* global Conf */
import React from 'react'
import { observer } from 'mobx-react'
import {Navbar, Nav, NavDropdown, MenuItem} from 'react-bootstrap'
import {ENTITIES} from '../constants'
import UserInfo from './userinfo'

const AppMenu = ({store}) => {
  function goTo (entityname, params = {}) {
    store.router.goTo(store.views.entity_list, {
      entityname: entityname
    }, store, Object.assign({_page: 1}, params))
  }
  const links = (
    <Nav>
      <NavDropdown eventKey={2} title={store.__('users')}>
        <MenuItem eventKey={2.1} onClick={() => goTo(ENTITIES.USERS)}>
          {store.__('users')}
        </MenuItem>
      </NavDropdown>
    </Nav>
  )

  return (
    <Navbar fixedTop fluid collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand className='cursor-pointer'>
          {Conf.siteName}
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        {store.loggedUser && links}
        <UserInfo store={store} />
      </Navbar.Collapse>
    </Navbar>
  )
}

export default observer(AppMenu)
