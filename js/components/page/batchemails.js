import React from 'react'
import PropTypes from 'prop-types'
import {observer, inject} from 'mobx-react'
import {Button} from 'react-bootstrap'
import TextInput from 'bstrap-react-mobx-admin/input/text'
import MDPreview from '../input/md_preview'

const BatchEmailsView = ({store}) => {
  //
  const message = store.cv.message ? (<div>{JSON.stringify(store.cv.message)}</div>) : null
  const errors = store.cv.errors
  const record = store.cv.record
  const updateData = store.cv.updateData.bind(store.cv)
  const disabled = store.cv.submitted || errors.size > 0

  return [(
    <h1>{store.__('batch emails')}</h1>
  ), (
    <div className='row'>
      <div className='col-xs-12'>
        <TextInput label={store.__('subject')} attr={'subject'} record={record}
          onChange={updateData} errors={errors} />
      </div>
    </div>
  ), (
    <div className='row'>
      <div className='col-xs-12 col-md-6'>
        <TextInput label={store.__('content')} attr={'content'} record={record}
          onChange={updateData} errors={errors}
          placeholder={store.__('start writing using markdown')}
          componentClass='textarea' rows='10' />
      </div>
      <div className='col-xs-12 col-md-6'>
        <MDPreview attr={'content'} record={record} />
      </div>
    </div>
  ), (
    <div className='row'>
      <div className='col-xs-12'>
        {message}

        <Button onClick={() => store.cv.sendBatchEmail()} disabled={disabled}>
          {store.__('send')}
        </Button>
      </div>
    </div>
  )]
}

BatchEmailsView.propTypes = {
  store: PropTypes.object.isRequired
}
export default inject('store')(observer(BatchEmailsView))
