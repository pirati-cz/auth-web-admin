import React from 'react'
import {observer, inject} from 'mobx-react'
import {ENTITIES} from '../../constants'

import Users from '../users/list'

const EntityList = ({store}) => {
  switch (store.router.params.entityname) {
    case ENTITIES.USERS: return <Users store={store} />
  }
  return null
}

export default inject('store')(observer(EntityList))
