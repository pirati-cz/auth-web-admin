import React from 'react'
import PropTypes from 'prop-types'
import {observer, inject} from 'mobx-react'
import {Button, FormControl} from 'react-bootstrap'

const LoginPage = ({store}) => {
  //
  function handleSubmit (e) {
    e.preventDefault()
    store.performLogin()
  }

  const error = store.cv.error !== null ? (<div>{store.cv.error}</div>) : null
  return (
    <div className='row'>
      <div className='col-xs-12 col-md-6'>
        <h1>{store.__('login')}</h1>

        <form>
          <FormControl name='uname' placeholder={store.__('username')}
            onChange={(e) => { store.cv.username = e.target.value }}
            value={store.cv.username} />
          <FormControl name='pwd' type='password' placeholder={store.__('password')}
            onChange={(e) => { store.cv.password = e.target.value }}
            value={store.cv.password} />
        </form>
        <hr style={{'marginTop': '3em'}} />
        {error}

        <Button onClick={handleSubmit} disabled={store.cv.submitted}>
          {store.__('login')}
        </Button>
      </div>
    </div>
  )
}

LoginPage.propTypes = {
  store: PropTypes.object.isRequired
}
export default inject('store')(observer(LoginPage))
