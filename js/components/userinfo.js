import React from 'react'
import { observer } from 'mobx-react'
import {Nav, NavItem} from 'react-bootstrap'

const UserInfo = ({store}) => {
  //
  function handleLogout (e) {
    e.stopPropagation()
    store.logout()
  }

  function handleLogin (e) {
    e.stopPropagation()
    store.goTo('login')
  }

  return (
    <Nav pullRight>
      <NavItem>
        {
          store.loggedUser ? (
            <div>
              <img src={store.loggedUser.picture} />
              {store.loggedUser.username}&nbsp;
              <button className='btn btn-xs' onClick={handleLogout}>
                {store.__('logout')}
              </button>
            </div>
          ) : (
            <button className='btn btn-xs' onClick={handleLogin}>
              {store.__('login')}
            </button>
          )
        }
      </NavItem>
    </Nav>
  )
}

export default observer(UserInfo)
