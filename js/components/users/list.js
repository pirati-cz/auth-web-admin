/* global alert */
import React from 'react'
import { observer } from 'mobx-react'
import TextField from 'react-mobx-admin/components/common/field/text'
import DateField from 'react-mobx-admin/components/common/field/date'
import ListView from 'bstrap-react-mobx-admin/view/list'
// for filters
import TextInput from 'bstrap-react-mobx-admin/input/text'
import SelectInput from 'bstrap-react-mobx-admin/input/select'

const CustomerListView = ({store}) => {
  //
  function batchActions () {
    // function _batchUnsubscribe () {
    //   if (confirm(store.__('are you sure you want all selected customers to unsubscribe from newsletter?'))) {
    //     store.deleteSelected()
    //   }
    // }
    function _gotoEmailing () {
      const ids = store.cv.selected_ids.join(',')
      store.goTo('batchemails', {ids})
    }
    function _showAddrList () {
      const addrs = store.cv.selectedItems.map(i => i.email).join(',')
      alert(addrs)
    }
    return [(
      <button className='btn btn-info' type='button' onClick={_gotoEmailing}>
        <span className='glyphicon glyphicon-letter' />&nbsp;{store.__('send email')}
      </button>
    ), (
      <button className='btn btn-warn' type='button' onClick={_showAddrList}>
        <span className='glyphicon glyphicon-letter' />&nbsp;{store.__('addresses')}
      </button>
    )]
  }

  const fields = [
    (attr, row) => (<TextField attr={attr} val={row[attr]} />),
    (attr, row) => {
      const DetailLink = ({text}) => (
        <a href='javascript:void(0)' onClick={() => store.detailClicked(row)}>{text}</a>
      )
      return (<TextField attr={attr} val={row[attr]} Component={DetailLink} />)
    },
    (attr, row) => (<TextField attr={attr} val={row[attr]} />),
    (attr, row) => (<DateField attr={attr} val={row[attr]} />),
    (attr, row) => (<TextField attr={attr} val={row[attr]} />),
    (attr, row) => (<TextField attr={attr} val={row[attr]} />),
    (attr, row) => (<TextField attr={attr} val={row[attr]} />),
    (attr, row) => (<TextField attr={attr} val={row[attr]} />),
    (attr, row) => (<TextField attr={attr} val={row[attr]} />)
  ]

  const filters = {
    'rank': {
      title: 'rank',
      icon: null,
      component: (props) => (
        <SelectInput {...props} optionsrecord={store.options} optionsattr={'userrank'} />
      )
    },
    'area': {
      title: 'area',
      icon: null,
      component: (props) => (
        <SelectInput {...props} optionsrecord={store.options} optionsattr={'areas'} />
      )
    },
    'name_like': {
      title: 'Name',
      icon: null,
      component: (props) => (<TextInput {...props} />)
    },
    'phone_like': {
      title: 'phone',
      icon: null,
      component: (props) => (<TextInput {...props} />)
    },
    'email_like': {
      title: 'email',
      icon: null,
      component: (props) => (<TextInput {...props} />)
    },
    'canhelp_like': {
      title: 'can help',
      icon: null,
      component: (props) => (<TextInput {...props} />)
    }
  }

  return (
    <ListView store={store.cv} fields={fields} filters={filters}
      batchActions={batchActions} />
  )
}
export default observer(CustomerListView)
