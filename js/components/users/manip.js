import React from 'react'
import TextInput from 'bstrap-react-mobx-admin/input/text'
import SelectInput from 'bstrap-react-mobx-admin/input/select'
import EditView from 'bstrap-react-mobx-admin/view/edit'

const EditForm = ({store}) => {
  const errors = store.cv.errors
  const record = store.cv.record
  const updateField = store.cv.updateData.bind(store.cv)

  return [(
    <div className='row'>
      <div className='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
        <TextInput label={store.__('name')} attr={'name'} record={record}
          onChange={updateField} errors={errors} />
        <SelectInput label={store.__('rank')} attr={'rank'}
          record={record} optionsrecord={store.options} onChange={updateField}
          optionsattr={'userrank'} />
        <TextInput label={store.__('can help')} attr={'canhelp'} record={record}
          onChange={updateField} errors={errors} />
      </div>
      <div className='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
        <TextInput label={store.__('phone')} attr={'phone'} record={record}
          onChange={updateField} errors={errors} />
        <TextInput label={store.__('email')} attr={'email'} record={record}
          onChange={updateField} errors={errors} />
        <TextInput label={store.__('zip')} attr={'psc'} record={record}
          onChange={updateField} errors={errors} />
      </div>
    </div>
  ), (
    <div className='row'>
      <div className='col-xs-12 col-sm-12'>
        <TextInput label={store.__('note')} attr={'note'} record={record}
          onChange={updateField} errors={errors}
          componentClass='textarea' rows='10' />
      </div>
    </div>
  )]
}

const UserEditView = ({store}) => (
  <EditView store={store.cv} onReturn2list={store.onReturn2list.bind(store)}>
    <EditForm store={store} />
  </EditView>
)
export default UserEditView
