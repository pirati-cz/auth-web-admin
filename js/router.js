import React from 'react'
import { Route } from 'mobx-router'

// components
import EntityList from './components/page/entity_list'
import EntityEdit from './components/page/entity_edit'
import LoginPage from './components/page/login'
import BatchEmailPage from './components/page/batchemails'
const prefix = '/'

const views = {
  login: new Route({
    path: prefix,
    component: <LoginPage />,
    beforeEnter: (route, params, store) => {
      store.showLogin()
    }
  }),
  entity_list: new Route({
    path: prefix + 'entity/:entityname',
    component: <EntityList />,
    beforeExit: (route, params, store, queryParams) => {
      store.beforeListViewExit(route, params, store, queryParams)
    },
    onEnter: (route, params, store) => {
      store.showEntityListView(params.entityname)
    },
    onParamsChange: (route, nextParams, store, nextQueryParams) => {
      store.onListParamsChange(nextParams, nextQueryParams)
    }
  }),
  entity_detail: new Route({
    path: prefix + 'entity/:entityname/:id',
    component: <EntityEdit />,
    onEnter: (route, params, store) => {
      store.showEntityUpdateView(params.entityname, params.id)
    },
    onParamsChange: (route, nextParams, store) => {
      store.showEntityUpdateView(nextParams.entityname, nextParams.id)
    }
  }),
  batchemails: new Route({
    path: prefix + 'batchemails/:ids',
    component: <BatchEmailPage />,
    onEnter: (route, params, store) => {
      store.showBatchEmailView()
    }
  })
}

export default views
