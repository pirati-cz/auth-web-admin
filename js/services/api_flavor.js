
export function convertQuery (q) {
  let converted = {
    _page: q._page,
    _pagesize: q._perPage,
    _sortCol: q._sortField,
    _sortOrder: q._sortDir
  }

  for (let i in q) {
    if (i[0] !== '_') converted[i] = q[i]
  }

  return converted
}

export function getTotalItems (response) {
  const h = response.headers['x-total-count']
  return h ? parseInt(h) : response.data.length
}
