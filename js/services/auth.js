/*  global localStorage */
import axios from 'axios'

const _LOC_KEY = 'pirati_admin_user'

export default class AuthService {

  constructor (apiUrl) {
    this.api_url = apiUrl
  }

  login (credentials) {
    return axios.post(`${this.api_url}/login`, credentials).then((response) => {
      localStorage.setItem(_LOC_KEY, JSON.stringify(response.data))
      return response.data
    })
  }

  getUserinfo () {
    // Retrieves the user token from localStorage
    return JSON.parse(localStorage.getItem(_LOC_KEY))
  }

  logout () {
    // Clear user token and profile data from localStorage
    localStorage.removeItem(_LOC_KEY)
  }
}
