/* global Conf */
import axios from 'axios'
import { convertQuery, getTotalItems } from './api_flavor'

const entityMapping = {
  'users': 'user'
}

class DataRequester {

  constructor (getHeaders, on401) {
    this.convertQuery = convertQuery
    this.getTotalItems = getTotalItems
    this.getHeaders = getHeaders
    this.on401 = on401
  }

  _defaultCatcher (err) {
    if (err.response && err.response.status === 401) {
      this.on401(err)
    }
    throw err // rethrow upwards
  }

  getEntries (entityName, params) {
    let qParams = this.convertQuery(params)

    const req = axios({
      method: 'get',
      url: `${Conf.api_url}/api/${entityMapping[entityName]}`,
      params: qParams,
      headers: this.getHeaders()
    })

    return req.then((response) => {
      return {
        data: response.data,
        totalItems: this.getTotalItems(response)
      }
    }).catch(this._defaultCatcher.bind(this))
  }

  getEntry (entityName, id, options = {}) {
    return axios({
      method: 'get',
      url: `${Conf.api_url}/api/${entityMapping[entityName]}/${id}`,
      headers: this.getHeaders()
    }).then((response) => {
      return response.data
    }).catch(this._defaultCatcher.bind(this))
  }

  deleteEntry (entityName, id) {
    return axios({
      method: 'delete',
      url: `${Conf.api_url}/api/${entityMapping[entityName]}/${id}`,
      headers: this.getHeaders()
    }).catch(this._defaultCatcher.bind(this))
  }

  saveEntry (entityName, data, id = null) {
    const conf = {
      headers: this.getHeaders(),
      data: data
    }

    if (id) {
      conf.url = `${this.apiUrl}/api/${entityMapping[entityName]}/${id}`
      conf.method = 'put'
    } else {
      conf.url = `${this.apiUrl}/api/${entityMapping[entityName]}`
      conf.method = 'post'
    }
    return axios(conf).then((response) => {
      return response.data
    }).catch(this._defaultCatcher.bind(this))
  }

  call (url, method = 'get', data) {   // call our API
    return axios({
      method: method,
      url: `${this.apiUrl}${url}`,
      headers: this.getHeaders(),
      data: data
    }).catch(this._defaultCatcher.bind(this))
  }
}

export default DataRequester
