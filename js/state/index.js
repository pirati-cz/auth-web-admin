import { observable, computed, action } from 'mobx'
import { RouterStore } from 'mobx-router'
import UserState from './user'
import cs from './i18n/cs'
import {ENTITIES} from '../constants'

import UsersStateInit from './views/users'
import BatchEmailsStateInit from './views/batchemailing'

export default class AppState extends UserState {
  //
  constructor(views) {
    super()
    this.router = new RouterStore()
    this.views = views
    this._add(ENTITIES.USERS, UsersStateInit)
  }

  _add (attr, ClassInit) {
    const klassews = ClassInit(this)
    this.manipStores[attr] = klassews.ManipState
    this.listStores[attr] = klassews.TableState
  }

  manipStores = {}
  listStores = {}

  @observable i18n = cs
  @observable lang = 'en'

  __(str) {
    return this.i18n[str] || str
  }

  @action
  changeLang(lang) {
    this.lang = lang
    this.i18n = {}
  }

  // map of all loaded options. In future can be managed in LUR manner
  @observable options = observable.map({
    'userstatus': [
      {value: 'enabled', label: 'enabled'},
      {value: 'disabled', label: 'disabled'}
    ],
    'userrank': [
      {value: 'subscriber', label: 'odebirac'},
      {value: 'supporter', label: 'podporovatel'},
      {value: 'wannabemember', label: 'uchazec'},
      {value: 'member', label: 'clen'}
    ],
    'areas': [
      {value: 'praha', label: 'Praha'},
      {value: 'strdc', label: 'Středočeský'},
      {value: 'karlv', label: 'Karlovarský'},
      {value: 'plzen', label: 'Plzeňský'},
      {value: 'ustec', label: 'Ústecký'},
      {value: 'jihoc', label: 'Jihočeský'},
      {value: 'liber', label: 'Liberecký'},
      {value: 'kralo', label: 'Královéhradecký'},
      {value: 'pardu', label: 'Pardubický'},
      {value: 'vysoc', label: 'Vysočina'},
      {value: 'morsl', label: 'Moravskoslezský'},
      {value: 'olomo', label: 'Olomoucký'},
      {value: 'zlins', label: 'Zlínský'},
      {value: 'jihmo', label: 'Jihomoravský'}
    ]
  })

  @observable messages = observable.map({})

  @action addMessage(text, type, timeout = 2000) {
    const message = {text, type, timeout}
    this.messages.set(text, message)
    if(timeout > 0) {
      function _remove() {
        this.messages.delete(text)
      }
      setTimeout(_remove.bind(this), timeout)
    }
    return message
  }

  @action removeMessage(message) {
    this.messages.delete(message.text)
  }


  onRequesterError(err) {
    this.addMessage(err.message, 'error')
  }

  // ---------------------------------------

  showEntityUpdateView(entityname, id) {
    const StoreClass = this.manipStores[entityname]
    if (StoreClass === undefined) {
      return this.on404('unknown entity ' + entityname)
    }
    id = (id && id !== '_new') ? id : null
    try {
      this.cv = new StoreClass(entityname, id, this.requester.saveEntry.bind(this.requester))
      this.cv.init()
      id && this.requester.getEntry(entityname, id).then(this.cv.onLoaded.bind(this.cv))
    } catch(err) {
      console.error(err)
    }
  }

  showEntityListView() {
    const entityname = this.router.params.entityname
    const StoreClass = this.listStores[entityname]
    if (StoreClass === undefined) {
      return this.on404('unknown entity ' + entityname)
    }
    this.cv = new StoreClass(entityname, this.requester, this.router, (newQPars) => {
      this.router.goTo(this.router.currentView, this.router.params, this, newQPars)
    })
    this.cv.init().catch(this.onRequesterError.bind(this))
  }

  beforeListViewExit() {
    const queryParamsBackup = Object.assign({}, this.router.queryParams)
    this.listQParamsBackup = queryParamsBackup
    this.listParamsBackup = Object.assign({}, this.router.params)
  }

  onListParamsChange(origParams, origQueryParams) {
    if (origParams.entityname !== this.router.params.entityname) {
      return this.showEntityListView()
    }
    return this.cv.init().catch(this.onRequesterError.bind(this))
  }

  detailClicked(row) {
    this.router.goTo(this.views.entity_detail, {
      entityname: this.router.params.entityname,
      id: row[this.cv.pkName || 'id']
    }, this)
  }

  goTo(view, params, queryParams={}) {
    this.router.goTo(this.views[view], params, this, queryParams)
  }

  addClicked() {
    this.router.goTo(this.views.entity_detail, {
      entityname: this.router.params.entityname,
      id: '_new'
    }, this)
  }

  showBatchEmailView() {
    const BatchEmailsState = BatchEmailsStateInit(this)
    this.cv = new BatchEmailsState(null, null)
    this.cv.init()
  }

  onReturn2list() {
    this.router.goTo(this.views.entity_list, this.listParamsBackup || {
      entityname: this.router.params.entityname
    }, this, this.listQParamsBackup || {_page: 1})
  }

}
