import {observable, computed, action, transaction} from 'mobx'
import DataRequester from '../services/data'
import AuthService from '../services/auth'

export default class UserState {

  constructor() {
    const on401 = this.on401.bind(this)
    const getHeaders = this.getHeaders.bind(this)
    this.requester = new DataRequester(getHeaders, on401)
    this.auth = new AuthService(Conf.api_url)
    const uinfo = this.auth.getUserinfo()
    if(uinfo === null) {
      this.loggedUser = null
      this.authToken = null
    } else {
      this.authToken = uinfo.token
      this.loggedUser = uinfo.user
    }
  }

  getHeaders () {
    return {
      'Authorization': `Bearer ${this.authToken}`
    }
  }

  @observable loggedUser = null
  @observable authToken = null

  @action
  showLogin() {
    this._showLogin()
  }

  _showLogin() {
    this.cv = observable({
      name: 'login',
      username: '',
      password: '',
      submitted: false,
      error: null
    })
  }

  @computed get userLoggedIn() {
    return this.loggedUser !== null
  }

  @computed get userIsAdmin() {
    return this.loggedUser !== null && this.loggedUser.user_metadata &&
      this.loggedUser.user_metadata.gid === Conf.adminGID
  }

  @computed get userIsOp() {
    return this.loggedUser !== null && this.loggedUser.user_metadata &&
      this.loggedUser.user_metadata.gid === Conf.opGID
  }

  on401() {
    this._do_logout()
    this.goTo('login')
  }

  @action
  performLogin() {
    this.cv.submitted = true
    this.cv.error = null
    return this.auth.login({
      username: this.cv.username,
      password: this.cv.password
    })
    .then((response) => {
      transaction(() => {
        this.authToken = response.token
        this.loggedUser = response.user
        this.cv.submitted = false
      })
      this.goTo('entity_list', {entityname: 'users'})
    })
    .catch((err) => {
      transaction(() => {
        this.cv.error = this.__('wrong username or password')
        this.cv.submitted = false
      })
    })
  }

  @action
  logout() {
    this._do_logout()
    this.goTo('login')
  }

  _do_logout() {
    this.auth.logout()
    this.loggedUser = null
    this.authToken = null
  }

}
