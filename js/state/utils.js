
export function lengthValidator(len, state) {
  return (val) => {
    if (!val || val.length === 0) {
      return state.__('this is mandatory')
    }
    if (val.length > len) {
      return state.__('too long')
    }
  }
}
