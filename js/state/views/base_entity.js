import DataManipState from 'react-mobx-admin/state/data_manip'
import DataTableState from 'react-mobx-admin/state/data_table'

export function BaseManipStateFn(store) {
  //
  return class BaseManipState extends DataManipState {
    //
    prepareNew() {
    }

    saveText = store.__('save')
    saveAndReturnText = store.__('save and return')
    cancelText = store.__('cancel')

    onSaved(saved) {
      store.addMessage('successfully saved', 'success', 2000)
      super.onSaved(saved)
    }
  }
}

export class BaseTableState extends DataTableState {
  perPage = Conf.perPage

  setDefaults() {
    const qp = this.router.queryParams
    qp._perPage = qp._perPage ? qp._perPage : localStorage.getItem('cargo_perPage') || this.perPage
    super.setDefaults()
  }
}
