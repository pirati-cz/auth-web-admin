import {observable, action} from 'mobx'
import DataManipState from 'react-mobx-admin/state/data_manip'

export default (store) => class BatchEmailsState extends DataManipState {
  //
  prepareNew() {
    this.record.set('subject', '')
    this.record.set('content', '')
  }

  validators = {
    'subject': (val) => {
      if (!val || val.length === 0) {
        return store.__('this is mandatory')
      }
    },
    'content': (val) => {
      if (!val || val.length === 0) {
        return store.__('this is mandatory')
      }
    }
  }
  @observable submitted = false
  @observable message = null

  @action sendBatchEmail() {
    this.submitted = true
    store.requester.call('/batchmail/send', 'post', this.record)
    .then((res) => {
      this.submitted = false
      this.message = res.data
      store.addMessage(store.__('mails sent'), 'info')
    }).catch((err) => {
      this.submitted = false
      store.onRequesterError(err)
    })
  }

}
