import { computed, observable } from 'mobx'

import {BaseManipStateFn, BaseTableState} from './base_entity'

export default (store) => {
  //
  class ManipState extends BaseManipStateFn(store) {

    edittitle = store.__('edit order')
    createtitle = store.__('create new order')
    validators = {
      'email': (val) => {
        if (val === null || val.length === 0) {
          return store.__('this is mandatory')
        }
      },
      'name': (val) => {
        if (val === null || val.length === 0) {
          return store.__('this is mandatory')
        }
      }
    }

    // init() {
    //   store.loadOptions('customers', '/customers?attrs=["name", "id"]')
    //   store.loadOptions('products', '/products?attrs=["id","name","price"]')
    //   return super.init()
    // }
    //
    // save() {
    //   this.record.set('total', this._countOrderPrice()) // update total
    //   return super.save()
    // }
  }

  class TableState extends BaseTableState {
    pkName = 'id'
    attrs = [
      'id', 'name', 'email',
      'created', 'rank', 'phone', 'psc', 'area', 'canhelp'
    ]
    headertitles = [
      'id', store.__('name'),
      store.__('email'), store.__('created'), store.__('rank'),
      store.__('phone'), store.__('zip'), store.__('area'),
      store.__('can help')
    ]
    title = store.__('users')
    defaultSortField = 'email'
    defaultSortDir = 'ASC'
    noSort = ['canhelp', 'phone']

    // init() {
    //   store.loadOptions('customers', '/customers?attrs=["name", "id"]')
    //   return super.init()
    // }
  }

  return {ManipState, TableState}
}
