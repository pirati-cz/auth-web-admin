require('dotenv').config()
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var babelOptions = {
  'presets': ['react', 'stage-0', ['es2015', {'modules': false}]],
  'plugins': [
    'transform-object-rest-spread',
    'transform-decorators-legacy',
    'transform-class-properties'
  ]
}

var conf = new HtmlWebpackPlugin({
  name: process.env.SITENAME || 'SSS admin',
  api_url: process.env.API_URL,
  basepath: process.env.BASEPATH || '',
  perPage: process.env.PERPAGE || 10,
  template: 'index.template.html',
  inject: false
})

module.exports = (env = {
  dev: true
}) => {
  const debug = env.dev
  if (!debug) {
    babelOptions.plugins.push('transform-react-remove-prop-types')
    console.log('============= PRODUCTION BUILD ======================')
  }

  const config = {
    devtool: debug ? 'inline-sourcemap' : false,
    entry: './js/main.js',
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /node_modules(?!(\\|\/)react-mobx-admin|(\\|\/)bstrap-react-mobx-admin|(\\|\/)mobx-router)/,
          loader: 'babel-loader',
          options: babelOptions
        }
      ]
    },
    output: {
      path: __dirname,
      filename: 'main.min.js'
    },
    plugins: debug ? [conf] : [
      // new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production')
        }
      }),
      conf
    ],
    externals: {
      'axios': 'axios',
      'mobx': 'mobx',
      'mobx-react': 'mobxReact',
      'react': 'React',
      'react-dom': 'ReactDOM'
    }
  }
  // console.log(JSON.stringify(config, null, 2))
  return config
}
